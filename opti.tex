\documentclass[12pt,oneside,letterpaper]{article}
\usepackage{sagetex}
\usepackage{amsmath}
\usepackage{hyperref}
\usepackage{tkz-berge}
\usepackage{color}
\definecolor{diff}{HTML}{336699}
\definecolor{eqn}{HTML}{336600}
\definecolor{other}{HTML}{990000}
\everymath{\displaystyle}
\title{How Many Teachers Does it Take To Carry A Blackboard Down A Hall?}
\author{Caspian Baskin}
\begin{document}
\maketitle

Two teachers are carrying a blackboard down a hall with a right-angle turn. The hall is 3m wide in one direction and 4m wide in the other.
\begin{sagesilent}
	var(x)
	theta=var('theta')
	g=(RDF(arctan((4/3)^(1/3))))
	t(x)=-(g)*x + (4+(g*3))
	T = plot(t(x),(x,0,(7+(4/5))),ymin=0, ymax=8, color='#990000')
	Q = text("3m",(1.5,4.2), rgbcolor=(0,0,0))
	H= line2d([(3,8),(3,4)], rgbcolor=(0,0,0))
	W = text("4m",(3.2,2), rotation=270.0, rgbcolor=(0,0,0))
	R=line([(3,4), (8,4)], rgbcolor=(0,0,0))
	I=arrow2d((0,4),(3,4),head=2,color='black')
	J=arrow2d((3,0),(3,4),head=2,color='black')
	oplot = I+J+R+H+T+Q+W
\end{sagesilent}
\begin{center} \sageplot[scale=.7]{oplot, aspect_ratio=1} \end{center}
	At first this looked to me to be an easy problem. I assumed that if I took the angle of the triangle made by half of the \(3\times4\) box, in the intersecting hallways, that that same angle would be the optimal angle for finding the length of the blackboard. Easy, and no calculus involved, but it doesn't work. Then I tried putting the outer corner of the hallway at the \(xy\) origin, and the corner at \((2,3)\), and using the Pythagorean Theorem with the axes and the length of the blackboard. But I couldn't find the right function. However, if the blackboard is fixed to the corner at \((3,4)\) and a variable length, so that it is always touching the walls, then it will always make two triangles in the space not occupied by the intersection of hallways. So if I find the hypotenuse of both of those triangles and add them together that gives me the length of the blackboard. Because the angle \(\theta\) is variable, I will have a function I can then differentiate to find the minimum value of \(\theta\), then use it in the function to find the maximum length of a blackboard that can fit through the corner. 

\begin{center}	\textbf{(A)} \end{center}
	The two triangles in question are complimentary as two sides are at right angles to each other and their hypotenuses are made by the same line. Because the \(\cos\theta=x\), and the \(\sin\theta=y\) , I can multiply the reciprocal of each function by the length of the known side and find the length of each hypotenuse. Then add these together, and I have a function for the length of the blackboard! Which, of course, means we can do some calculus. \pagebreak 
\begin{sagesilent}
	B(theta)=(sec(theta)*3) + (csc(theta)*4)
	D = diff(B(theta))
	d = plot(D, (x, 0, (pi/2)), ticks=pi/8, tick_formatter=pi/8, ymin=-40, ymax=40, color='#336699', detect_poles=True, typeset='latex')
	b = plot(B(theta), (x,0,pi), ticks=pi/8, tick_formatter=pi/8, ymin=-40, ymax=40, color='#336600', detect_poles=True, typeset='latex')
	G = d+b
	m = (3*sec(g)) + (4*csc(g))
\end{sagesilent}
\begin{center} \textbf{(B)}\\
\sageplot[scale=.5]{b} \end{center}
\begin{align*} 
	\textcolor{eqn}{f(x)=3sec\theta+4csc\theta}
\end{align*}
\begin{center} \textbf{(C)} \end{center}
	From the graph we can see that as the black board approaches \(\frac{\pi}{2}\) or \(90^{\circ}\) it ceases to exist, due to the impossibility of it touching the corner and the opposite wall at the same time at \(\frac{\pi}{2}\). We can disregard points beyond \(\frac{\pi}{2}\), and now I can differentiate the function with respect to \(\theta\), for \(0<x<\tfrac{\pi}{2}\).
\begin{align*}
	f(x)&=3\sec\theta+4\csc\theta \\
	D_{\theta}f&=3\sec\theta\tan\theta-4\csc\theta\cot\theta \\
	D_{\theta}f&=3\left(\frac{1}{\cos\theta}\frac{\sin\theta}{\cos\theta}\right)-4\left(\frac{1}{\sin\theta}\frac{\cos\theta}{\sin\theta}\right)\\
	D_{\theta}f&=3\left(\frac{\sin\theta}{\cos^2\theta}\right)-4\left(\frac{\cos\theta}{\sin^2\theta}\right)\\
\end{align*}
\begin{center} \sageplot[scale=.5]{G} \end{center}
	\begin{align*}
	\textcolor{diff}{D_{\theta}f=3\left(\frac{\sin\theta}{\cos^2\theta}\right)-4\left(\frac{\cos\theta}{\sin^2\theta}\right)}
\end{align*}
\begin{align*}
	\textcolor{eqn}{f(x)=3sec\theta+4csc\theta}
\end{align*}
Now I can set \(D_{\theta}f\) equal to zero, and find \(f(x)\)'s critical numbers.
\begin{align*}
	0&=3\left(\frac{\sin\theta}{\cos^2\theta}\right)-4\left(\frac{\cos\theta}{\sin^2\theta}\right)\\
	3\left(\frac{\sin\theta}{\cos^2\theta}\right)&=4\left(\frac{\cos\theta}{\sin^2\theta}\right)\\
	3\left(\frac{\frac{\sin\theta}{\cos^2\theta}}{\frac{\cos\theta}{\sin^2\theta}}\right)&=4\\
	\left(\frac{\sin\theta}{\cos^2\theta}\right)\left(\frac{\sin^2\theta}{\cos\theta}\right)&=\frac{4}{3}\\
	\tan^3\theta&=\frac{4}{3}\\
	\tan\theta&=\sqrt[\leftroot{-2}\uproot{3}3]{\tfrac{4}{3}}
\end{align*}
Now I can find \(\theta\) at \(D_{\theta}f\)'s relative minimum when \(0<x<\tfrac{\pi}{2}\).
\begin{align*}
	\arctan(\sqrt[\leftroot{-2}\uproot{3}3]{\tfrac{4}{3}})&=\sage{g}.
\end{align*}
All that's left to do is to substitute \sage{g} into the original equation to find the length of the blackboard.
\begin{equation*}
	3\sec(\sage{g})+4\csc(\sage{g})\\
	= \sage{m}
\end{equation*}
\begin{sagesilent}

	z = point((g,m), pointsize=40, color='#990000')
	n = point((g,0), pointsize=40, color='#990000')
	V = text("(.83,9.87)",(g,14), rgbcolor=(0,0,0))

	N = G + z + V
\end{sagesilent}
\begin{center} \sageplot[scale=.5]{N} \end{center}
	In the real world however, despite the thickness of the blackboard which we have not taken into account, due to the flexibility of a \(\approx\) 10m blackboard, I would have to say you could get a slightly longer one through the corner with a little "finesse".
\begin{sagesilent}
	J = T+H+R
\end{sagesilent}
\begin{center} \sageplot[scale=.4]{J, aspect_ratio=1} \end{center}
\begin{align*}
	\textcolor{other}{b(x)=-.833271859855x+.49981557957}
\end{align*}

\end{document}

